//
//  TSCoreDataStoreTests.m
//  TSCoreDataStoreTests
//
//  Created by Richard Moult on 12/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "TSCoreDataStore.h"

@interface TSCoreDataStoreTests : XCTestCase
@end



@implementation TSCoreDataStoreTests

- (void)setUp {
    
    [super setUp];
}


#pragma mark - init


- (void)testInit_shouldThrow {
    
    XCTAssertThrows([TSCoreDataStore new]);
}

                     
- (void)testInitInMemory_withoutModelName_shouldThrow {
    
    XCTAssertThrows([[TSCoreDataStore alloc] initInMemoryStoreWithModelName:nil]);
}


- (void)testInitPersistent_withoutModelName_shouldThrow {
    
    XCTAssertThrows([[TSCoreDataStore alloc] initPersistentStoreWithModelName:nil]);
}


- (void)testInitInMemory_withModelName_shouldReturnObject {
    
    XCTAssertNotNil([[TSCoreDataStore alloc] initInMemoryStoreWithModelName:@"TSCoreDataStore"]);
}


- (void)testInitPersistent_withModelName_shouldReturnObject {
    
    XCTAssertNotNil([[TSCoreDataStore alloc] initPersistentStoreWithModelName:@"TSCoreDataStore"]);
}


#pragma mark - managed object contexts


- (void)testInitInMemory_allContextCanBeCreated {
    
    TSCoreDataStore *dataStore = [[TSCoreDataStore alloc] initInMemoryStoreWithModelName:@"TSCoreDataStore"];
    XCTAssertNotNil(dataStore.mainManagedObjectContext);
    XCTAssertNotNil(dataStore.privateManagedObjectContext);
}


- (void)testInitPersistent_allContextCanBeCreated {
    
    TSCoreDataStore *dataStore = [[TSCoreDataStore alloc] initPersistentStoreWithModelName:@"TSCoreDataStore"];
    XCTAssertNotNil(dataStore.mainManagedObjectContext);
    XCTAssertNotNil(dataStore.privateManagedObjectContext);
}


- (void)testInitInMemory_createPrivateMOCWithMainAsParent {
    
    TSCoreDataStore *dataStore = [[TSCoreDataStore alloc] initInMemoryStoreWithModelName:@"TSCoreDataStore"];
    XCTAssertEqualObjects(dataStore.privateManagedObjectContext.parentContext, dataStore.mainManagedObjectContext);
}


- (void)testInitPersistent_createPrivateMOCWithMainAsParent {
    
    TSCoreDataStore *dataStore = [[TSCoreDataStore alloc] initPersistentStoreWithModelName:@"TSCoreDataStore"];
    XCTAssertEqualObjects(dataStore.privateManagedObjectContext.parentContext, dataStore.mainManagedObjectContext);
}

#pragma mark - save 


- (void)testInitInMemory_privateMOCOnSaveShouldMergeChanges {
    
    TSCoreDataStore *dataStore = [[TSCoreDataStore alloc] initInMemoryStoreWithModelName:@"TSCoreDataStore"];
    
    id mockedMainMOC = [self mockedAndExpectMergeMainMOCFromDataStore:dataStore];
    
    [self waitAndSaveMOC:dataStore.privateManagedObjectContext];
    
    XCTAssertNoThrow([mockedMainMOC verify]);
}


#pragma mark - helpers

- (void)waitAndSaveMOC:(NSManagedObjectContext *)context {
    
    [context performBlockAndWait:^{
        [context save:nil];
    }];
}

- (id)mockedAndExpectMergeMainMOCFromDataStore:(TSCoreDataStore *)dataStore {
    
    id mockedMainMOC = [OCMockObject partialMockForObject:dataStore.mainManagedObjectContext];
    [[mockedMainMOC expect] mergeChangesFromContextDidSaveNotification:OCMOCK_ANY];
    return mockedMainMOC;
}

@end
